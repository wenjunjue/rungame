﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkinView : View {
    [SerializeField]
    public Renderer ballSkin, charSkin;
    public override string Name
    {
        get
        {
            return Consts.V_PlayerSkin;
        }
    }

    public override void HandleEvent(string eventName, object data)
    {
        
    }

    void Awake() {
        var index = GetModel<GlobalModel>().BallSkinIndex;
        var skin = SkinManager.Instance.GetBallSkin(index);
        if (skin != null)
        {
            ballSkin.material = skin;
        }

        index = GetModel<GlobalModel>().CharSkinIndex;
        skin = SkinManager.Instance.GetCharSkin(index);
        if (skin != null)
        {
            charSkin.material = skin;
        }
    }
}
