﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinManager : MonoSingleton<SkinManager>
{

    [SerializeField]
    List<Material> charList = new List<Material>();
    [SerializeField]
    List<Material> ballList = new List<Material>();
    public Material GetCharSkin(int index)
    {
        if (index < charList.Count)
            return charList[index];
        else
            return null;
    }

    public Material GetBallSkin(int index)
    {
        if (index < ballList.Count)
            return ballList[index];
        else
            return null;
    }
}
