﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {
    int a;
    float timer;
	// Use this for initialization
	void Start () {
        StartCoroutine(Count(a));
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (timer > 2) {
            a++;
            StartCoroutine(Count(a));
            timer = 0;
        }
	}
    IEnumerator Count(int index) {
        int i = 0;
        while (true) {
            Debug.Log(index+":"+i);
            i++;
            yield return new WaitForSeconds(1);
        }
    }
}
