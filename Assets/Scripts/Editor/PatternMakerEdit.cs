﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PatternMakerEdit : EditorWindow {

    [MenuItem("Tools/Make")]
    static void MakePattern() {
        PatternManager patternManager = GameObject.Find("ObjectManager").GetComponent<PatternManager>();
        Debug.Log("MakePattern!!!!!");
        var items = Selection.gameObjects[0].transform;
        //创建一条新的方案
        var newPattern = new Pattern();
        foreach (var e in items) {
            Transform child = e as Transform;
            Debug.Log(child.name);
           var prefab= PrefabUtility.GetPrefabParent(child.gameObject);
            var ele=new Element();
            ele.name = prefab.name;
            ele.pos = child.localPosition;
            newPattern.elementList.Add(ele);

        }
        patternManager.patternList.Add(newPattern);
    }
}
