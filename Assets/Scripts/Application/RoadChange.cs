﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadChange : MonoBehaviour {

    public GameObject currentRoad;
    public GameObject nextRoad;

	void Start () {

        int i = Random.Range(1, 5);
        int j = Random.Range(1, 5);
        currentRoad = ObjectPoolManager.Instance.Spawn("Road/Pattern_" + i);
        nextRoad = ObjectPoolManager.Instance.Spawn("Road/Pattern_" + j);
        currentRoad.transform.position = Vector3.zero;
        nextRoad.transform.position = currentRoad.transform.position + new Vector3(0, 0, 160);
        //PatternManager.Instance.SpawnPattern(currentRoad.transform.Find("Items"));
        //PatternManager.Instance.SpawnPattern(nextRoad.transform.Find("Items"));
    }
	
    void OnTriggerEnter(Collider other) {
        if (other.tag==Tags.Road) {
            Change();
        
        }
    }

    void Change() {
        ObjectPoolManager.Instance.UnSpawn(currentRoad);
        currentRoad = nextRoad;
        int i = Random.Range(1, 5);
        nextRoad= ObjectPoolManager.Instance.Spawn("Road/Pattern_" + i);
        nextRoad.transform.position = currentRoad.transform.position + new Vector3(0, 0, 160);
        //PatternManager.Instance.SpawnPattern(nextRoad.transform.Find("Items"));
    }

  

}
