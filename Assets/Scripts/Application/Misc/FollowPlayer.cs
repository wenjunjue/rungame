﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    Transform player;
    Vector3 m_offset;
	void Start () {
        player = GameObject.FindWithTag(Tags.Player).transform;
        m_offset = transform.position - player.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        Vector3 targetPos = player.position + m_offset;
        transform.position = targetPos;
	
	}
}
