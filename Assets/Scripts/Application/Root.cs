﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

//整个程序的入口
public class Root : MonoSingleton<Root>
{
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        //注册了第一个Controller，用来响应启动游戏事件
        MVC.RegisterController(Consts.E_StartUp, typeof(StartUpController));
        //启动游戏事件
        MVC.SendEvent(Consts.E_StartUp, null);
    }
    public void LoadScene(int level)
    {
        //发起关闭当前场景的事件
        ScenesArgs data = new ScenesArgs();
        data.sceneIndex = SceneManager.GetActiveScene().buildIndex;
        MVC.SendEvent(Consts.E_ExitScene, data);
        Debug.Log("关闭场景" + data.sceneIndex);
        //加载场景
        SceneManager.LoadScene(level, LoadSceneMode.Single);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mod)

    {
        Debug.Log("打开场景" + scene.buildIndex);
        //发起打开新场景的事件
        ScenesArgs data = new ScenesArgs();
        data.sceneIndex = scene.buildIndex;
        MVC.SendEvent(Consts.E_EnterScene, data);
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    public  void DelayInvoke(Action action, float delaySeconds)
    {
        StartCoroutine(DelayFuc(action, delaySeconds));
    }
    IEnumerator DelayFuc(Action action, float delaySeconds)
    {
        yield return new WaitForSeconds(delaySeconds);
        action();
    }
}
public class ScenesArgs
{

    public int sceneIndex;
}

