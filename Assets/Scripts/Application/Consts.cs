﻿public static class Consts
{

    //系统事件
    public const string E_ExitScene = "E_ExitScene";//退出场景
    public const string E_EnterScene = "E_EnterScene";//进入场景
    public const string E_StartUp = "E_StartUp";//启动游戏


    //游戏中被动事件
    public const string E_RuntimePassiveEvent = "E_RuntimePassiveEvent";//游戏中被动事件
    //商城事件
    public const string E_ShopEvent= "E_ShopEvent";//商城事件

    //玩家输入事件
    public const string E_SlideInput = "E_SlideInput";//手势输入
    public const string E_UIInput = "E_UIInput";//UI输入


    //Model更新事件
    public const string E_ModelUpdate = "E_ModelUpdate";//Model数据更新事件
    public const string E_GameStateUpdate = "E_GameStateUpdate";//游戏状态更新事件
    
    //视图
    public const string V_PlayerMove = "V_PlayerMove";
    public const string V_SlideInput = "V_SlideInput";
    public const string V_PlayerAnim = "V_PlayerAnim";
    public const string V_PlayerCollision = "V_PlayerCollision";
    public const string V_Magnet = "V_Magnet";
    public const string V_ShootBall = "V_ShootBall";
    public const string V_GameTimer = "V_GameTimer";
    public const string V_PlayerSkin = "V_PlayerSkin";

    //UI视图
    public const string V_GamePanelView = "V_GamePanelView";
    public const string V_ScorePanelView = "V_ScorePanelView";
    public const string V_FailPanelView = "V_FailPanelView";
    public const string V_PausePanelView = "V_PausePanelView";
    public const string V_ShopPanelView = "V_ShopPanelView";
    public const string V_HomePanelView = "V_HomePanelView";
    public const string V_SettingPanelView = "V_SettingPanelView";
    public const string V_BuyToolPanelView = "V_BuyToolPanelView";
    //模型
    public const string M_RuntimeModel = "M_RuntimeModel";
    public const string M_GlobalModel = "M_GlobalModel";

}
