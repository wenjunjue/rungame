﻿public static class Tags {

    public const string Road = "Road";
    public const string Player = "Player";
    public const string Fence = "Fence";//栅栏，碰了减速
    public const string Block = "Block";//禁区，碰了死亡
    public const string NearPlayerArea = "NearPlayerArea";//检测玩家靠近的区域
    public const string People = "People";
    public const string Item = "Item";
    public const string MagnetTrigger = "MagnetTrigger";
    public const string GoalKeeper = "GoalKeeper";
    public const string QiuMen = "QiuMen";
}
