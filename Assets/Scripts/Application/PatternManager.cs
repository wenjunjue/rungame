﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 方案管理器
/// </summary>
public class PatternManager : MonoSingleton<PatternManager>
{
    public List<Pattern> patternList = new List<Pattern>();

    public void SpawnPattern(Transform items)
    {
        //随机一套方案
        var index = Random.Range(0, patternList.Count);
        var pattern = patternList[index];
        //将方案中的物体都租过来
        for (int i = 0; i < pattern.elementList.Count; i++)
        {
            var ele = pattern.elementList[i];
            //var item = Instantiate(GetPrefab(ele.name), pool);
            var item = ObjectPoolManager.Instance.Spawn("Item/"+ele.name);
            item.transform.parent = items;
            item.transform.localPosition = ele.pos;

        }

    }
}


//一套方案
[System.Serializable]
public class Pattern
{
    public List<Element> elementList = new List<Element>();
}

//方案中的元素
[System.Serializable]
public class Element
{
    public string name;
    public Vector3 pos;
}
