﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : ReUsableObject
{
    public override void OnSpawn()
    {
        base.OnSpawn();
    }

    public override void OnUnSpawn() {
        base.OnUnSpawn();
    }



    void Update()
    {
        //转动
        transform.Rotate(new Vector3(0, 5, 0));
    }

    public void CollisionPlayer()
    {

        //特效
        var FX = ObjectPoolManager.Instance.Spawn("Effect/FX_JinBi");
        FX.transform.position = transform.position;
        Root.Instance.DelayInvoke(
            () => { 
            ObjectPoolManager.Instance.UnSpawn(FX);
        }, 
            0.5f);
        //声音
        SoundManager.Instance.PlayEffect("Se_UI_JinBi");
        //发消息
        RuntimeEventArgs args = new RuntimeEventArgs();
        args.effect = RuntimeEvent.GetMoney;
        MVC.SendEvent(Consts.E_RuntimePassiveEvent, args);
        //回收
        //ObjectPoolManager.Instance.UnSpawn(gameObject);
        Root.Instance.DelayInvoke(
            () => {
                ObjectPoolManager.Instance.UnSpawn(gameObject);
            },
            0);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == Tags.MagnetTrigger)
        {
            StartCoroutine(Move());
        }
    }
    IEnumerator Move()
    {
        float timer = 0;
        Transform player = GameObject.FindWithTag(Tags.Player).transform;
        while (Vector3.Distance(transform.position,player.position)>0.01f)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, 0.4f);
            timer += Time.deltaTime;
            yield return 0;
        }
    }
}
