﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Double : ReUsableObject
{
    Transform effectPool;
    public override void OnSpawn() {
        base.OnSpawn();
    }

    public override void OnUnSpawn() {
        base.OnUnSpawn();
    }


    void Awake()
    {
        effectPool = GameObject.Find("EffectPool").transform;
    }
    void Update()
    {
        //转动
        transform.Rotate(new Vector3(0, 5, 0));
    }


    public void CollisionPlayer()
    {

        //特效
        var FX = ObjectPoolManager.Instance.Spawn("Effect/FX_JinBi");
        FX.transform.position = transform.position;
        Root.Instance.DelayInvoke(
            () => {
                ObjectPoolManager.Instance.UnSpawn(FX);
            },
            0.5f);

        //声音
        SoundManager.Instance.PlayEffect("Se_UI_Stars");
        //发消息
        RuntimeEventArgs args = new RuntimeEventArgs();
        args.effect = RuntimeEvent.GetDouble;
        MVC.SendEvent(Consts.E_RuntimePassiveEvent, args);
        //回收
        ObjectPoolManager.Instance.UnSpawn(this.gameObject);

    }
}
