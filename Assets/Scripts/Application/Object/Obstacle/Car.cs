﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : ReUsableObject
{

    float moveSpeed = 20;
    public override void OnSpawn()
    {
        base.OnSpawn();
    }

    public override void OnUnSpawn()
    {
        base.OnUnSpawn();
    }


    IEnumerator Move()
    {
        float timer = 0;
        while (timer<2)
        {
            transform.position += transform.forward * moveSpeed * Time.deltaTime;
            timer += Time.deltaTime;
            yield return 0;
        }
    }
    //当玩家靠近时
    public void NearPlayer()
    {
        StartCoroutine(Move());
    }


}
