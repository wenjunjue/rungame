﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalKeeper : MonoBehaviour {

    Animation anim;

    void Start() {
        anim = GetComponentInChildren<Animation>();
    }
    //当碰到玩家时
    public void CollisionPlayer()
    {
        StartCoroutine("Fly");

    }
    IEnumerator Fly()
    {
        anim.Play("fly");
        transform.forward = -Vector3.forward;
        float timer = 0;
        while (timer < 2)
        {
            transform.position += (Vector3.forward + Vector3.up) * 5 * Time.deltaTime;
            timer += Time.deltaTime;
            yield return 0;
        }
        ObjectPoolManager.Instance.UnSpawn(this.gameObject);
        
    }
}
