﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShootGoal : ReUsableObject {

    [SerializeField]
    Transform goalKeeper;//守门员
    float moveSpeed = 4;
    public override void OnSpawn()
    {
        base.OnSpawn();
    }

    public override void OnUnSpawn()
    {
        base.OnUnSpawn();
    }

    //当玩家靠近时
    public void NearPlayer()
    {
        //发消息
        RuntimeEventArgs args = new RuntimeEventArgs();
        args.effect = RuntimeEvent.ReadyShoot;
        MVC.SendEvent(Consts.E_RuntimePassiveEvent, args);
        StartCoroutine(Move());
    }

    public void HitBall() {
        StopAllCoroutines();
    }


    IEnumerator Move()
    {
        Vector3 dir = Vector3.left;
        while (true)
        {
            if (goalKeeper.position.x > 2) {
                dir = Vector3.left;
            }
            else if (goalKeeper.position.x < -2)
            {
                dir = Vector3.right;
            }
            goalKeeper.position += dir * moveSpeed * Time.deltaTime;
            yield return 0;
        }
    }

}
