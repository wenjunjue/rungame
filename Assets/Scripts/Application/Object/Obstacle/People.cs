﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class People : ReUsableObject
{
    Animation anim;
    float moveSpeed = 5;
    public override void OnSpawn()
    {
        base.OnSpawn();
    }

    public override void OnUnSpawn()
    {
        base.OnUnSpawn();
    }

    void Awake()
    {
        anim = GetComponentInChildren<Animation>();
        anim.Stop();
    }
    IEnumerator Move()
    {
        anim.Play("run");
        float timer = 0;
        while (timer < 2)
        {
            transform.position += transform.forward * moveSpeed * Time.deltaTime;
            timer += Time.deltaTime;
            yield return 0;
        }
        
        ObjectPoolManager.Instance.UnSpawn(gameObject);
    }
    //当玩家靠近时
    public void NearPlayer()
    {
        StartCoroutine("Move");
    }

    //当碰到玩家时
    public void CollisionPlayer()
    {

        StopCoroutine("Move");
        StartCoroutine("Fly");

    }
    IEnumerator Fly()
    {
        anim.Play("fly");
        transform.forward = -Vector3.forward;
        float timer = 0;
        while (timer < 2)
        {
            transform.position += (Vector3.forward + Vector3.up) * 5 * Time.deltaTime;
            timer += Time.deltaTime;
            yield return 0;
        }
        ObjectPoolManager.Instance.UnSpawn(this.gameObject);
        
    }
}
