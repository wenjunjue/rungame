﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : ReUsableObject
{
    Transform itemsTrans;
    void Awake()
    {
        itemsTrans = transform.Find("Items");
    }
    public override void OnSpawn()
    {
        base.OnSpawn();
        ////获取一套方案
        ////根据方案租用物体
        PatternManager.Instance.SpawnPattern(itemsTrans);
    }

    public override void OnUnSpawn()
    {
        base.OnUnSpawn();
        //归还方案里租用的物体;
        while (itemsTrans.childCount > 0)
        {
            ObjectPoolManager.Instance.UnSpawn(itemsTrans.GetChild(0).gameObject);
        }
    }

}
