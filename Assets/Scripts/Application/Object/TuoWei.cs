﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//球的特效
public class TuoWei : MonoBehaviour {

    void OnTriggerEnter(Collider other) {
        if (other.tag == Tags.GoalKeeper) {
            //特效
            var FX = ObjectPoolManager.Instance.Spawn("Effect/FX_ZhuangJi"); 
            FX.transform.position = transform.position;
            Root.Instance.DelayInvoke(
                    () => { ObjectPoolManager.Instance.UnSpawn(FX); }
                    , 0.5f);

            other.transform.parent.parent.parent.SendMessage("HitBall");
            //事件
            RuntimeEventArgs data = new RuntimeEventArgs();
            data.effect = RuntimeEvent.GoalKeeper;
            MVC.SendEvent(Consts.E_RuntimePassiveEvent, data);
            //回收
            ObjectPoolManager.Instance.UnSpawn(gameObject);
            Root.Instance.DelayInvoke(
                ()=> { ObjectPoolManager.Instance.UnSpawn(other.transform.parent.parent.parent.gameObject); }
                ,1);
        }
        else if (other.tag==Tags.QiuMen) {
            //特效
            var FX= ObjectPoolManager.Instance.Spawn("Effect/FX_GOAL");
            FX.transform.position = transform.position+Vector3.up;

            //声音
            SoundManager.Instance.PlayEffect("Se_UI_Goal");
            //事件
            RuntimeEventArgs data = new RuntimeEventArgs();
            data.effect = RuntimeEvent.Goal;
            MVC.SendEvent(Consts.E_RuntimePassiveEvent,data);
            //回收
            ObjectPoolManager.Instance.UnSpawn(gameObject);
            ObjectPoolManager.Instance.UnSpawn(other.transform.parent.parent.gameObject);
        }
    }
}
