﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//玩家撞机视图
public class PlayerCollision : MonoBehaviour {

    [SerializeField]
    GameObject qiuwang;

    void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.name);
        if (other.tag == Tags.Fence)
        {
            RuntimeEventArgs data = new RuntimeEventArgs();
            data.effect = RuntimeEvent.LoseSpeed;
            Hit(data);
        }
        else if (other.tag == Tags.Block)
        {
            RuntimeEventArgs data = new RuntimeEventArgs();
            data.effect = RuntimeEvent.Die;
            Hit(data);
        }
        else if (other.tag == Tags.NearPlayerArea)
        {
            other.transform.parent.SendMessage("NearPlayer", SendMessageOptions.DontRequireReceiver);
        }
        else if (other.tag == Tags.People)
        {
            RuntimeEventArgs data = new RuntimeEventArgs();
            data.effect = RuntimeEvent.LoseSpeed;
            Hit(data);
            other.SendMessage("CollisionPlayer", SendMessageOptions.DontRequireReceiver);
        }
        else if (other.tag == Tags.Item)
        {//触发捡到道具
            other.SendMessage("CollisionPlayer", SendMessageOptions.DontRequireReceiver);
        }
        else if (other.tag == Tags.QiuMen)
        {
            Debug.Log("撞击球门");
            qiuwang.SetActive(true);
            StartCoroutine(ZhuangjiQiuWang());

        }
        else if (other.tag == Tags.GoalKeeper) {
            RuntimeEventArgs data = new RuntimeEventArgs();
            data.effect = RuntimeEvent.LoseSpeed;
            Hit(data);
            other.SendMessage("CollisionPlayer", SendMessageOptions.DontRequireReceiver);
        }
    }
    IEnumerator ZhuangjiQiuWang() {
        qiuwang.SetActive(true);
        yield return new WaitForSeconds(2);
        qiuwang.SetActive(false);
    }

    void Hit(RuntimeEventArgs data) {
        //播放一个撞击的特效
        GameObject FX = ObjectPoolManager.Instance.Spawn("Effect/FX_ZhuangJi"); 
        FX.transform.position = transform.position;

        MVC.SendEvent(Consts.E_RuntimePassiveEvent, data);//发送玩家碰撞事件
    }
}

