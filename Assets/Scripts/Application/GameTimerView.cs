﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimerView : View {
    public override string Name
    {
        get
        {
            return Consts.V_GameTimer;
        }
    }

    public override void HandleEvent(string eventName, object data)
    {
    }

    // Use this for initialization
    void Start () {
        global = GetModel<GlobalModel>();
        runtime = GetModel<RuntimeModel>();
	}
    GlobalModel global;
    RuntimeModel runtime;
	// Update is called once per frame
	void Update () {
        if(global.State==GameState.Play)
            runtime.Update();
	}
}
