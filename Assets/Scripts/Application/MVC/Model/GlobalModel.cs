﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
//游戏状态model
public class GlobalModel : Model
{
    GameState state = GameState.Load;
    int money = 1000;//玩家金钱
    int level = 1;//玩家等级
    float exp;//玩家经验

    int charSkinIndex = 1, ballSkinIndex = 1;

    public override string Name
    {
        get
        {
            return Consts.M_GlobalModel;
        }
    }

    public GameState State
    {
        get
        {
            return state;
        }
        set
        {
            state = value;

            MVC.SendEvent(Consts.E_GameStateUpdate, this);
        }
    }

    public int BallSkinIndex
    {
        get
        {

            return ballSkinIndex;
        }

        set
        {
            ballSkinIndex = value;
            PlayerPrefs.SetInt("ballSkinIndex", ballSkinIndex);
        }
    }

    public int CharSkinIndex
    {
        get
        {

            return charSkinIndex;
        }

        set
        {
            charSkinIndex = value;
            PlayerPrefs.SetInt("charSkinIndex", charSkinIndex);
        }
    }

    public int Money
    {
        get
        {
            return money;
        }

        set
        {
            money = value;
            PlayerPrefs.SetInt("Money", money);
        }
    }

    public int Level
    {
        get
        {
            return level;
        }
    }


    public float Exp
    {
        get
        {
            return exp;
        }
    }

    public void GetExp(float expValue)
    {
        //升级需要的经验
        int max = 50 * Level;
        exp += expValue;
        while (Exp > max)
        {
            level += 1;
            exp -= max;
            max = 50 * Level;
        }
        PlayerPrefs.SetInt("Level", level);
        PlayerPrefs.SetFloat("Exp", exp);
    }

    //读取本地存档
    public void Read()
    {
        ballSkinIndex = PlayerPrefs.GetInt("ballSkinIndex");
        charSkinIndex = PlayerPrefs.GetInt("charSkinIndex");
        exp = PlayerPrefs.GetFloat("Exp");
        level = PlayerPrefs.GetInt("Level");
        money = PlayerPrefs.GetInt("Money");
    }
    public void Bribery(int cost)
    {

    }
}
public enum GameState
{
    Load, Home, Play, Pause, Die, Score, Shop
}