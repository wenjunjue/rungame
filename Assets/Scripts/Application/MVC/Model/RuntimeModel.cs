﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//游戏状态model
public class RuntimeModel : Model
{
    public int goalNum;//进球数量
    public int deadNum;//死亡次数

    public int coinNum;//金币数量

    public int doubleNum;//双倍道具的数量
    public float doubleTimer;//双倍计时器

    public int magnetNum;//磁铁道具的数量
    public float magnetTimer;//磁铁计时器

    public int whistleNum;//无敌道具的数量
    public float whistleTimer;//无敌计时器

    public bool ballActive;//球的数量

    public float time;//游戏计时

    
    
    public float ballTimer;//足球计时器
   
    public float distance;//跑出的距离

    public override string Name
    {
        get
        {
            return Consts.M_RuntimeModel;
        }
    }
    //贿赂的价格
    public int BriberyPrice
    {
        get
        {
            return (int)(500 * Mathf.Pow(2, deadNum - 1));
        }
    }

    //每次开启新游戏时，还原运行时数据
    public void Reset()
    {
        Debug.Log("重置运行时数据");
        goalNum = 0;
        deadNum = 0;
        coinNum = 0;
        doubleNum = 0;
        magnetNum = 0;
        whistleNum = 0;
        ballActive = false;
        time = 0;
        doubleTimer = 0;
        whistleTimer = 0;
        ballTimer = 0;
        magnetTimer = 0;
        distance = 0;
    }
    public void Die()
    {
        deadNum++;
    }
    //进球
    public void GetGoal()
    {
        goalNum++;
        ModelUpdateArgs data = new ModelUpdateArgs("goalNum", goalNum.ToString());
        MVC.SendEvent(Consts.E_ModelUpdate, data);
    }


    public void UseMagnet()
    {
        magnetTimer += 2;
        GetMagnet(-1);
    }
    public void GetMagnet(int num = 1)
    {
        magnetNum += num;
        ModelUpdateArgs data = new ModelUpdateArgs("magnetNum", magnetNum.ToString());
        MVC.SendEvent(Consts.E_ModelUpdate, data);
    }

    internal void UseWhistle()
    {
        Debug.Log("无敌");
        whistleTimer += 5;
        GetWhistle(-1);
    }

    public void GetWhistle(int num = 1)
    {
        whistleNum += num;
        ModelUpdateArgs data = new ModelUpdateArgs("whistleNum", whistleNum.ToString());
        MVC.SendEvent(Consts.E_ModelUpdate, data);
    }
    public void GetSuiji(int num = 1)
    {
        int r=Random.Range(0, 3);
        switch (r)
        {
            case 0:
                whistleNum += num;
                break;
            case 1:
                magnetNum += num;
                break;
            case 2:
                doubleNum += num; 
                break;
            default:
                break;
        }

    }
    public void UseBall()
    {
        ballActive = false;
        ballTimer = 0;
        ModelUpdateArgs data = new ModelUpdateArgs("ballActive", ballActive.ToString());
        MVC.SendEvent(Consts.E_ModelUpdate, data);
    }
    public void GetBall()
    {
        ballActive = true;
        ModelUpdateArgs data = new ModelUpdateArgs("ballActive", ballActive.ToString());
        MVC.SendEvent(Consts.E_ModelUpdate, data);
        ballTimer = 3;
    }
    //捡到星星
    public void UseDouble()
    {
        doubleTimer += 5;
        GetDouble(-1);
    }
    public void GetDouble(int num = 1)
    {
        doubleNum += num;
        ModelUpdateArgs data = new ModelUpdateArgs("doubleNum", doubleNum.ToString());
        MVC.SendEvent(Consts.E_ModelUpdate, data);
    }

    //捡到金币
    public void GetCoin()
    {
        if (doubleTimer > 0)
            coinNum += 2;
        else
            coinNum += 1;
        ModelUpdateArgs data = new ModelUpdateArgs("coin", coinNum.ToString());
        MVC.SendEvent(Consts.E_ModelUpdate, data);
    }
    public void Update()
    {
        time += Time.deltaTime;
        MVC.SendEvent(Consts.E_ModelUpdate, new ModelUpdateArgs("time", time.ToString("0.00")));
        if (doubleTimer > 0)
        {
            doubleTimer -= Time.deltaTime;
            if (doubleTimer < 0)
                doubleTimer = 0;
            ModelUpdateArgs data = new ModelUpdateArgs("doubleTime", doubleTimer.ToString("0.0"));
            MVC.SendEvent(Consts.E_ModelUpdate, data);
        }
        if (magnetTimer > 0)
        {
            magnetTimer -= Time.deltaTime;
            if (magnetTimer < 0)
            {
                magnetTimer = 0;
            }
            ModelUpdateArgs data = new ModelUpdateArgs("magnetTime", magnetTimer.ToString("0.0"));
            MVC.SendEvent(Consts.E_ModelUpdate, data);
        }
        if (whistleTimer > 0)
        {
            whistleTimer -= Time.deltaTime;
            if (whistleTimer < 0)
                whistleTimer = 0;
            ModelUpdateArgs data = new ModelUpdateArgs("whistleTime", whistleTimer.ToString("0.0"));
            MVC.SendEvent(Consts.E_ModelUpdate, data);
        }


        if (ballTimer > 0)
        {
            ballTimer -= Time.deltaTime;
            if (ballTimer < 0)
            {
                UseBall();
                ballTimer = 0;
            }
            ModelUpdateArgs data = new ModelUpdateArgs("ballTime", ballTimer.ToString());
            MVC.SendEvent(Consts.E_ModelUpdate, data);
        }
    }

    public void GetDistance(float value)
    {
        distance = value;
        ModelUpdateArgs data = new ModelUpdateArgs("distance", distance.ToString("0"));
        MVC.SendEvent(Consts.E_ModelUpdate, data);
    }
}

public class ModelUpdateArgs
{
    public string attName;
    public string value;
    public ModelUpdateArgs(string a, string b = "")
    {
        attName = a;
        value = b;
    }
}