﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePanelView : View
{
    public override string Name
    {
        get
        {
            return Consts.V_GamePanelView;
        }
    }
    void Awake()
    {
        RegisterAttentionEvent();
        UpdateUI();
        btnPause.onClick.AddListener(() =>
    {
        MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Game_Pause"));
    });
        btnDouble.onClick.AddListener(() =>
    {
        MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Game_Double"));
    });
        btnMagnet.onClick.AddListener(() =>
    {
        MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Game_Magnet"));
    });
        btnWhistle.onClick.AddListener(() =>
    {
        MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Game_Whistle"));
    });
        btnBall.onClick.AddListener(() =>
    {
        MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Game_Ball"));
    });
    }

    private void UpdateUI()
    {
        var runtime = GetModel<RuntimeModel>();
        UpdateMagnetNum(runtime.magnetNum.ToString());
        UpdateDoubleNum(runtime.doubleNum.ToString());
        UpdateWhistleNum(runtime.whistleNum.ToString());
        UpdateDoubleTime("0");
        UpdateMagnetTime("0");
        UpdateWhistleTime("0");
        UpdateTime("0");
    }

    public override void RegisterAttentionEvent()
    {
        AttentionList.Add(Consts.E_ModelUpdate);
    }
    public override void HandleEvent(string eventName, object data)
    {
        if (eventName == Consts.E_ModelUpdate)
        {
            var args = data as ModelUpdateArgs;
            switch (args.attName)
            {
                case "coin":
                    UpdateCoin(args.value); ;
                    break;
                case "distance":
                    UpdateDistance(args.value);
                    break;
                case "magnetTime":
                    UpdateMagnetTime(args.value);
                    break;
                case "whistleTime":
                    UpdateWhistleTime(args.value);
                    break;
                case "doubleTime":
                    UpdateDoubleTime(args.value);
                    break;
                case "doubleNum":
                    UpdateDoubleNum(args.value);
                    break;
                case "magnetNum":
                    UpdateMagnetNum(args.value);
                    break;
                case "whistleNum":
                    UpdateWhistleNum(args.value);
                    break;
                case "ballActive":
                    UpdateBallActive(args.value);
                    break;
                case "ballTime":
                    UpdateBallPower(args.value);
                    break;
                case "time":
                    UpdateTime(args.value);
                    break;
            }
        }
    }


    [SerializeField]
    Button btnPause, btnBall, btnWhistle, btnDouble, btnMagnet;

    [SerializeField]
    Text txtCoin, txtDistance, txtTime, txtWhisleTime, txtDoubleTime, txtMagnetTime, txtDoubleNum, txtWhislteNum, txtMagnetNum;

    [SerializeField]
    Slider sliderTime;

    [SerializeField]
    Image imgBallPower, imgWhistle, imgDouble, imgMagnet, imgStart321;

    [SerializeField]
    List<Sprite> start321spriteList = new List<Sprite>();
    void UpdateCoin(string value)
    {
        txtCoin.text = value;
    }

    void UpdateDistance(string value)
    {
        txtDistance.text = value + "米";
    }
    void UpdateTime(string value)
    {
        txtTime.text = value + "s";
    }
    void UpdateMagnetTime(string value)
    {
        float v;
        if (float.TryParse(value, out v))
        {
            if (v <= 0)
                txtMagnetTime.transform.parent.gameObject.SetActive(false);
            else
            {
                txtMagnetTime.transform.parent.gameObject.SetActive(true);
                txtMagnetTime.text = value + "s";
            }
        }

    }
    void UpdateWhistleTime(string value)
    {
        float v;
        if (float.TryParse(value, out v))
        {
            if (v <= 0)
                txtWhisleTime.transform.parent.gameObject.SetActive(false);
            else
            {
                txtWhisleTime.transform.parent.gameObject.SetActive(true);
                txtWhisleTime.text = value + "s";
            }
        }

    }

    void UpdateDoubleTime(string value)
    {
        float v;
        if (float.TryParse(value, out v))
        {
            if (v <= 0)
                txtDoubleTime.transform.parent.gameObject.SetActive(false);
            else
            {
                txtDoubleTime.transform.parent.gameObject.SetActive(true);
                txtDoubleTime.text = value + "s";
            }
        }

    }

    private void UpdateDoubleNum(string value)
    {
        int v;
        if (int.TryParse(value, out v))
        {
            if (v > 0)
            {
                btnDouble.gameObject.SetActive(true);
                txtDoubleNum.text = "x" + value;
            }
            else
            {
                btnDouble.gameObject.SetActive(false);
            }

        }
    }
    private void UpdateMagnetNum(string value)
    {
        int v;
        if (int.TryParse(value, out v))
        {
            if (v > 0)
            {
                btnMagnet.gameObject.SetActive(true);
                txtMagnetNum.text = "x" + value;
            }
            else
            {
                btnMagnet.gameObject.SetActive(false);
            }

        }
    }


    private void UpdateWhistleNum(string value)
    {
        int v;
        if (int.TryParse(value, out v))
        {
            if (v > 0)
            {
                btnWhistle.gameObject.SetActive(true);
                txtWhislteNum.text = "x" + value;
            }
            else
            {
                btnWhistle.gameObject.SetActive(false);
            }

        }
    }

    private void UpdateBallActive(string value)
    {
        bool v;
        if (bool.TryParse(value, out v))
        {
            if (v)
            {
                btnBall.gameObject.SetActive(true);
                imgBallPower.fillAmount = 1;
            }
            else
            {
                btnBall.gameObject.SetActive(false);
                imgBallPower.fillAmount = 0;
            }

        }
    }

    void UpdateBallPower(string value)
    {
        float v;
        if (float.TryParse(value, out v))
        {
            imgBallPower.fillAmount = v / 3;

        }
    }

    void Start()
    {
        StartCoroutine(Start321());
    }
    IEnumerator Start321()
    {
        //3
        imgStart321.sprite = start321spriteList[0];
        SoundManager.Instance.PlayEffect("Se_UI_Button");
        yield return new WaitForSeconds(1);
        //2
        imgStart321.sprite = start321spriteList[1];
        SoundManager.Instance.PlayEffect("Se_UI_Button");
        yield return new WaitForSeconds(1);
        //1
        imgStart321.sprite = start321spriteList[2];
        SoundManager.Instance.PlayEffect("Se_UI_Button");
        yield return new WaitForSeconds(1);
        imgStart321.gameObject.SetActive(false);
        SoundManager.Instance.PlayEffect("Se_UI_Button");
        MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Pause_Continue"));
        yield return 0;
    }
}
