﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FailPanelView : UIView
{

    [SerializeField]
    Text txtPrice;
    [SerializeField]
    Button btnBribery, btnEnd;

    public override string Name
    {
        get
        {
            return Consts.V_FailPanelView;
        }
    }
    void Awake()
    {
        btnBribery.onClick.AddListener(()=>
    {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Fail_Bribery"));
        });
        btnEnd.onClick.AddListener(()=>
    {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Fail_End"));
        });
    }

    public override void HandleEvent(string eventName, object data)
    {

    }
 
    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
        //查询复活需要的价格
        int price = GetModel<RuntimeModel>().BriberyPrice;
        txtPrice.text = price.ToString();


        //查询玩家的金钱数量
        int money = GetModel<GlobalModel>().Money;
        //判断钱够不够
        if (money > price)
        {
            btnBribery.gameObject.SetActive(true);
        }
        else
        {
            btnBribery.gameObject.SetActive(false);
        }
    }
}
