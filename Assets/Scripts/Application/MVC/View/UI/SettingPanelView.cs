﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingPanelView : UIView {

    [SerializeField]
    Slider music, effect;

    [SerializeField]
    Button btnClose;
    public override string Name {
        get {
            return Consts.V_SettingPanelView;
        }
    }

    public override void HandleEvent(string eventName, object data)
    {
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }


    void Awake() {
        music.onValueChanged.AddListener((float value)=>{
            SoundManager.Instance.SetMusicVolume(value);
        });
        effect.onValueChanged.AddListener((float value) => {
            SoundManager.Instance.SetEffectVolume(value);
        });
        btnClose.onClick.AddListener(() =>
        {
            Hide();
        });
    }
}
