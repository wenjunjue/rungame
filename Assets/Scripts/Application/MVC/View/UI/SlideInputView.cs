﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//手势输入
public class SlideInputView : View
{


    bool activeInput = false;
    Vector3 m_mousePos;


    public override string Name
    {
        get
        {
            return Consts.V_SlideInput;
        }
    }



    //获取输入方向
    private void GetInputDirection()
    {
        //获取手势
        if (Input.GetMouseButtonDown(0))
        {
            activeInput = true;
            m_mousePos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0) && activeInput)
        {
            Vector3 dir = Input.mousePosition - m_mousePos;
            if (dir.magnitude > 20)
            {
                if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
                {
                    if (dir.x > 0)
                    {
                        //往右
                        MVC.SendEvent(Consts.E_SlideInput, new SlideInputArgs("Right"));
                    }
                    else
                    {
                        //往左
                        MVC.SendEvent(Consts.E_SlideInput, new SlideInputArgs("Left"));
                    }
                }
                else
                {
                    if (dir.y > 0)
                    {
                        //往上
                        MVC.SendEvent(Consts.E_SlideInput, new SlideInputArgs("Up"));
                    }
                    else
                    {
                        //往下
                        MVC.SendEvent(Consts.E_SlideInput, new SlideInputArgs("Down"));
                    }
                }
                activeInput = false;
            }

        }

        //获取键盘
        if (Input.GetKeyDown(KeyCode.W))
        {
            //往上
            MVC.SendEvent(Consts.E_SlideInput, new SlideInputArgs("Up"));
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            //往左
            MVC.SendEvent(Consts.E_SlideInput, new SlideInputArgs("Left"));
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            //往下
            MVC.SendEvent(Consts.E_SlideInput, new SlideInputArgs("Down"));
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            //往右
            MVC.SendEvent(Consts.E_SlideInput, new SlideInputArgs("Right"));

        }
    }


    void Update()
    {
        GetInputDirection();
    }

    public override void HandleEvent(string eventName, object data)
    {
        throw new System.NotImplementedException();
    }

}
public class SlideInputArgs
{
    public string direction = "";
    public SlideInputArgs(string dir)
    {
        direction = dir;
    }
}
