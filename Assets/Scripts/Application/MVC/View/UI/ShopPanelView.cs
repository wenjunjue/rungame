﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopPanelView : UIView
{
    [SerializeField]
    Toggle tgChar1, tgChar2, tgChar3, tgTShirt1, tgTShirt2, tgTShirt3, tgBall1, tgBall2, tgBall3;

    [SerializeField]
    Button btnZhuangbei,btnStart,btnBack;
    [SerializeField]
    public Renderer charRenderer, ballRenderer;

    public int CharSkinIndex {
        get {
           return (charIndex-1) * 3 + TShirtIndex-1;
        }
    }
    public int BallSkinIndex;
    int charIndex = 1;
    int TShirtIndex = 1;

    void Awake()
    {
        tgChar1.onValueChanged.AddListener(OnTgChar1Change);
        tgChar2.onValueChanged.AddListener(OnTgChar2Change);
        tgChar3.onValueChanged.AddListener(OnTgChar3Change);
        tgTShirt1.onValueChanged.AddListener(OnTgTShirt1Change);
        tgTShirt2.onValueChanged.AddListener(OnTgTShirt2Change);
        tgTShirt3.onValueChanged.AddListener(OnTgTShirt3Change);
        tgBall1.onValueChanged.AddListener(OnTgBall1Change);
        tgBall2.onValueChanged.AddListener(OnTgBall2Change);
        tgBall3.onValueChanged.AddListener(OnTgBall3Change);
        btnZhuangbei.onClick.AddListener(()=> {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Shop_Zhuangbei"));
        });
        btnStart.onClick.AddListener(()=> {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Shop_Start"));
        });
        btnBack.onClick.AddListener(()=> {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Shop_Back"));
        });
    }
    public void OnTgChar1Change(bool value)
    {
        charIndex = 1;
        switch (TShirtIndex)
        {
            case 1:
                charRenderer.material = SkinManager.Instance.GetCharSkin(0);
                break;
            case 2:
                charRenderer.material = SkinManager.Instance.GetCharSkin(1);
                break;
            case 3:
                charRenderer.material = SkinManager.Instance.GetCharSkin(2);
                break;
        }

    }
    public void OnTgChar2Change(bool value)
    {
        charIndex = 2;
        switch (TShirtIndex)
        {
            case 1:
                charRenderer.material = SkinManager.Instance.GetCharSkin(3);
                break;
            case 2:
                charRenderer.material = SkinManager.Instance.GetCharSkin(4);
                break;
            case 3:
                charRenderer.material = SkinManager.Instance.GetCharSkin(5);
                break;
        }
    }
    public void OnTgChar3Change(bool value)
    {
        charIndex = 3;
        switch (TShirtIndex)
        {
            case 1:
                charRenderer.material = SkinManager.Instance.GetCharSkin(6);
                break;
            case 2:
                charRenderer.material = SkinManager.Instance.GetCharSkin(7);
                break;
            case 3:
                charRenderer.material = SkinManager.Instance.GetCharSkin(8);
                break;
        }
    }


    public void OnTgTShirt1Change(bool value)
    {
        TShirtIndex = 1;
        switch (charIndex)
        {
            case 1:
                charRenderer.material = SkinManager.Instance.GetCharSkin(0);
                break;
            case 2:
                charRenderer.material = SkinManager.Instance.GetCharSkin(3);
                break;
            case 3:
                charRenderer.material = SkinManager.Instance.GetCharSkin(6);
                break;
        }

    }
    public void OnTgTShirt2Change(bool value)
    {
        TShirtIndex = 2;
        switch (charIndex)
        {
            case 1:
                charRenderer.material = SkinManager.Instance.GetCharSkin(1);
                break;
            case 2:
                charRenderer.material = SkinManager.Instance.GetCharSkin(4);
                break;
            case 3:
                charRenderer.material = SkinManager.Instance.GetCharSkin(7);
                break;
        }
    }
    public void OnTgTShirt3Change(bool value)
    {
        TShirtIndex = 3;
        switch (charIndex)
        {
            case 1:
                charRenderer.material = SkinManager.Instance.GetCharSkin(2);
                break;
            case 2:
                charRenderer.material = SkinManager.Instance.GetCharSkin(5);
                break;
            case 3:
                charRenderer.material = SkinManager.Instance.GetCharSkin(8);
                break;
        }
    }
    public void OnTgBall1Change(bool value)
    {
        ballRenderer.material = SkinManager.Instance.GetBallSkin(0);
        BallSkinIndex = 0;
    }
    public void OnTgBall2Change(bool value)
    {

        ballRenderer.material = SkinManager.Instance.GetBallSkin(1);
        BallSkinIndex = 1;
    }
    public void OnTgBall3Change(bool value)
    {
        ballRenderer.material = SkinManager.Instance.GetBallSkin(2);
        BallSkinIndex = 2;

    }


    public override string Name
    {
        get
        {
            return Consts.V_ShopPanelView;
        }

    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void HandleEvent(string eventName, object data)
    {
    }
}
