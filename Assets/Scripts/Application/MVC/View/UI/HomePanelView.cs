﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomePanelView : UIView
{
    [SerializeField]
    Button btnStart, btnSetting, btnShop;
    public override string Name
    {
        get { return Consts.V_HomePanelView; }
    }

    public override void HandleEvent(string eventName, object data)
    {

    }

    public void Awake() {
        btnShop.onClick.AddListener(()=> {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Home_Shop"));
        });
        btnSetting.onClick.AddListener(() => {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Home_Setting"));
        });
        btnStart.onClick.AddListener(() => {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Home_Start"));
        });
    }
    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }


}
