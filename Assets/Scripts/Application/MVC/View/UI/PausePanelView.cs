﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PausePanelView : UIView
{
    [SerializeField]
    Button btnContinue, btnHome;

    [SerializeField]
    Text txtCoin, txtDistance, txtScore, txtGoal;


    public override string Name
    {
        get
        {
            return Consts.V_PausePanelView;
        }
    }
    void Awake()
    {
        btnContinue.onClick.AddListener(() =>
    {
        MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Pause_Continue"));
    });
        btnHome.onClick.AddListener(() =>
        {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Pause_Home"));
        });
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
        RuntimeModel runtime = GetModel<RuntimeModel>();
        txtCoin.text = runtime.coinNum.ToString("0");
        txtDistance.text = runtime.distance.ToString("0") + "米";
        txtGoal.text = runtime.goalNum.ToString("0") + "个";
        txtScore.text = (runtime.distance + runtime.coinNum * 10 + runtime.goalNum * 100).ToString("0");
    }

    public override void HandleEvent(string eventName, object data)
    {

    }
}
