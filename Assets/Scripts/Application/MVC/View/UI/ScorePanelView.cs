﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScorePanelView : UIView
{


    [SerializeField]
    Text txtScore, txtDistance, txtCoin, txtGoal, txtLevel, txtEXP;
    [SerializeField]
    Button btnShop, btnHome, btnReStart;
    [SerializeField]
    Slider slider;
    public override string Name
    {
        get
        {
            return Consts.V_ScorePanelView;
        }
    }

    public override void HandleEvent(string eventName, object data) { }

    void Awake()
    {
        btnReStart.onClick.AddListener(()=> {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Score_Restart"));
        });
        btnShop.onClick.AddListener(()=> {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Score_Shop"));
        });
        btnHome.onClick.AddListener(()=> {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("Score_Home"));
        });
    }

    IEnumerator GetExp(float value)
    {
        int level = GetModel<GlobalModel>().Level;
        float exp = GetModel<GlobalModel>().Exp + value;

        while (exp > 0)
        {
            
            slider.maxValue = 50 * level;
            float speed = slider.maxValue / 50;
            slider.value = Mathf.MoveTowards(slider.value, slider.maxValue, speed);

            exp -= speed;
            //Debug.Log(slider.value+":"+exp);
            txtEXP.text = slider.value.ToString() + "/" + slider.maxValue.ToString();
            txtLevel.text = level.ToString() + "级";
            if (slider.value >= slider.maxValue)
            {
                level += 1;
                slider.maxValue = 50 * level;
                slider.value = 0;
            }
            yield return 0;
        }
    }


    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
        var model = GetModel<RuntimeModel>();
        txtCoin.text = model.coinNum.ToString();
        txtGoal.text = model.goalNum.ToString();
        txtDistance.text = model.distance.ToString("0");
        txtScore.text = (
            model.distance +
            model.coinNum * 10 +
            model.goalNum * 100
            ).ToString("0");
        int value = int.Parse(txtScore.text);
        //Debug.Log("经验值" + value);
        StartCoroutine(GetExp(value));
        GetModel<GlobalModel>().GetExp(value);
        GetModel<GlobalModel>().Money += model.coinNum;

    }
}
