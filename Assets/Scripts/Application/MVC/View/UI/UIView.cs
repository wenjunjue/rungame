﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIView : View {
    public abstract void Hide();
    public abstract void Show();

}
