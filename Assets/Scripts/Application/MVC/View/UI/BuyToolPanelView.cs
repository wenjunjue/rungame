﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyToolPanelView : UIView
{

    [SerializeField]
    Button btnBuySuiji, btnBuyWhistle, btnBuyMagnet, btnBuyDouble, btnBack, btnStart;
    [SerializeField]
    Text txtSuiji, txtWhistle, txtMagnet, txtDouble;
    [SerializeField]
    Text txtSuijiPrice, txtWhistlePrice, txtMagnetPrice, txtDoublePrice, txtMoney;

    GlobalModel global;
    int magnetNum, doubleNum, whistleNum, suijiNum;

    public void Awake()
    {
        global = GetModel<GlobalModel>();
        UpdateUI();
        btnBuySuiji.onClick.AddListener(() =>
        {
            int price = int.Parse(txtSuijiPrice.text);
            if (global.Money > price)
            {
                MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("BuySuiji"));
                global.Money -= price;
                UpdateUI();
                suijiNum++;
                txtSuiji.text = suijiNum.ToString();
            }
        });
        btnBuyWhistle.onClick.AddListener(() =>
        {
            int price = int.Parse(txtWhistlePrice.text);
            if (global.Money > price) {
                MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("BuyWhistle"));
                global.Money -= price;
                UpdateUI();
                whistleNum++;
                txtWhistle.text = whistleNum.ToString();
            }   
        }




        );
        btnBuyMagnet.onClick.AddListener(() =>
        {
            int price = int.Parse(txtMagnetPrice.text);
            if (global.Money > price)
            {
                MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("BuyMagnet"));
                global.Money -= price;
                UpdateUI();
                magnetNum++;
                txtMagnet.text = magnetNum.ToString();
            }
        });
        btnBuyDouble.onClick.AddListener(() =>
        {
            int price = int.Parse(txtDoublePrice.text);
            if (global.Money > price)
            {
                MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("BuyDouble"));
                global.Money -= price;
                UpdateUI();
                doubleNum++;
                txtDouble.text = doubleNum.ToString();
            }
        });
        btnBack.onClick.AddListener(() =>
        {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("BuyTool_Back"));
        });
        btnStart.onClick.AddListener(() =>
        {
            MVC.SendEvent(Consts.E_UIInput, new UIInputArgs("BuyTool_Start"));
        });
    }
    public override string Name
    {
        get
        {
            return Consts.V_BuyToolPanelView;
        }
    }

    public override void HandleEvent(string eventName, object data)
    {

    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
        UpdateUI();
    }
    public void UpdateUI() {
        Debug.Log("更新金币");
        txtMoney.text = global.Money.ToString("5000");
    }


}
