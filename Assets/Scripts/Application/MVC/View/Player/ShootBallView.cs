﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBallView : View
{

    [SerializeField]
    GameObject ball;
    [SerializeField]
    GameObject tuowei;
    bool shooting;

    public override string Name
    {
        get
        {
            return Consts.V_ShootBall;
        }
    }

    public override void HandleEvent(string eventName, object data) { }

    // Update is called once per frame
    void Update()
    {
        if (GetModel<GlobalModel>().State != GameState.Play)
            return;
        if (!shooting)
            return;
        if (tuowei.transform.position.y > 0.6f)
        {
            tuowei.transform.position = Vector3.MoveTowards(tuowei.transform.position, new Vector3(tuowei.transform.position.x, 0.6f, tuowei.transform.position.z), 30 * Time.deltaTime);
        }
        tuowei.transform.position += Vector3.forward * 30 * Time.deltaTime;
    }

    internal void Shoot()
    {
        //隐藏球
        ball.SetActive(false);
        //生成拖尾特效
        tuowei = ObjectPoolManager.Instance.Spawn("Effect/FX_TuoWei_01");
        shooting = true;
        tuowei.transform.position = transform.position;
        
    }
    public void Init()
    {
        shooting = false;
        ball.SetActive(true);
    }
}
