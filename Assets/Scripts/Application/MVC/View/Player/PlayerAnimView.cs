﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimView : View
{
    Animation anim;
    #region 属性
    public override string Name
    {
        get
        {
            return Consts.V_PlayerAnim;
        }
    }
    public string State
    {
        get
        {
            if (!anim.isPlaying)
                return "";
            else
                return anim.clip.name;
        }
    }
    #endregion

    #region 方法
    public override void RegisterAttentionEvent()
    {
    }

    //播放一段动画，播完后执行某个方法
    IEnumerator Play(string clipName, Action callBack)
    {
        anim.Play(clipName);
        anim.clip = anim.GetClip(clipName);
        anim.Play();
        while (true)
        {
            if (anim.clip.name != clipName)
                break;
            if (!anim.isPlaying)
            {
                if (callBack != null)
                    callBack.Invoke();
                else
                    anim.Stop();
            }
            yield return 0;
        }
    }
    public void PlayRun()
    {
        //Debug.Log("播放跑步动画");
        anim.clip = anim.GetClip("run");
        anim.Play();
    }
    public void RunLeft()
    {

        //Debug.Log("播放左跳动画");
        StartCoroutine(Play("left_jump", PlayRun));
        SoundManager.Instance.PlayEffect("Se_UI_Huadong");

    }

    public void RunRight()
    {

        //Debug.Log("播放右跳动画");
        StartCoroutine(Play("right_jump", PlayRun));
        SoundManager.Instance.PlayEffect("Se_UI_Huadong");

    }
    public void Jump()
    {

        //Debug.Log("播放跳动画");
        StartCoroutine(Play("jump", PlayRun));
        SoundManager.Instance.PlayEffect("Se_UI_Jump");

    }
    public void Roll()
    {

        //Debug.Log("播放翻滚动画");
        StartCoroutine(Play("roll", PlayRun));
        SoundManager.Instance.PlayEffect("Se_UI_Huadong");

    }
    public void PlayShoot()
    {
        if (State == "jump")
            StartCoroutine(Play("Shoot02", PlayRun));
        else
            StartCoroutine(Play("Shoot01", PlayRun));
        SoundManager.Instance.PlayEffect("Se_UI_Huadong");

    }
    public void Die()
    {
        //Debug.Log("播放死亡动画");
        //播放一个撞机的声音
        SoundManager.Instance.PlayEffect("Se_UI_Hit");
        StartCoroutine(Play("Collision", () =>
        {
            StartCoroutine(Play("die", () => { }));
            //播放一个死亡的声音
            SoundManager.Instance.PlayEffect("Se_UI_End");
        }));


    }
    public void Hit()
    {
        Debug.Log("播放撞击动画");
        StartCoroutine(Play("Collision", PlayRun));
        //播放一个撞机的声音
        SoundManager.Instance.PlayEffect("Se_UI_Hit");
    }
    #endregion

    #region Unity回调
    void Awake()
    {
        anim = GetComponent<Animation>();
        RegisterAttentionEvent();

    }
    #endregion

    #region 事件回调

    public override void HandleEvent(string eventName, object data)
    {
    }
    #endregion

}
