﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetView : View
{

    GameObject magnetTrigger;
    public override string Name
    {
        get
        {
            return Consts.V_Magnet;
        }
    }


    void Awake()
    {
        magnetTrigger = transform.Find("MagnetTrigger").gameObject;
        RegisterAttentionEvent();
    }
    public override void RegisterAttentionEvent()
    {
        AttentionList.Add(Consts.E_ModelUpdate);
    }
    public override void HandleEvent(string eventName, object data)
    {
        if (eventName == Consts.E_ModelUpdate) {
            var args = data as ModelUpdateArgs;
            if (args.attName=="magnetTime") {
                Debug.Log("magnetTime="+args.value);
            }
            
            if (args.attName == "magnetTime" && args.value == "0.0") {
                CloseMagnet();
            }
        }
    }

    public void UseMagnet()
    {
        magnetTrigger.SetActive(true);
    }
    public void CloseMagnet() {
        magnetTrigger.SetActive(false);
    }
}

