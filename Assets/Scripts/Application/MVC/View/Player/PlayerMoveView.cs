﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//主角移动
public class PlayerMoveView : View
{

    #region 字段
    public float moveSpeed;
    CharacterController cc;

    int currentIndex = 1;
    int targetIndex;
    float targetPosX;
    float jumpTimer = -1;
    float speedAddition = 0.5f;
    float normalSpeed;//正常速度
    bool isPlaying = false;
    float dropSpeed = 10;
    #endregion
    float startZ = 0;
    #region 属性
    public override string Name
    {
        get
        {
            return Consts.V_PlayerMove;
        }
    }
    #endregion

    #region 方法
    /// <summary>
    /// 注册关心事件列表
    /// </summary>
    public override void RegisterAttentionEvent()
    {
        AttentionList.Add(Consts.E_GameStateUpdate);
    }

    //允许向前跑
    void Run()
    {
        //向前移动
        cc.Move(transform.forward * moveSpeed * Time.deltaTime);


    }

    //往左移动
    public void RunLeft()
    {
        if (currentIndex > 0)
        {
            targetIndex = currentIndex - 1;
            targetPosX = (targetIndex - 1) * 2;
            currentIndex = targetIndex;
        }
    }
    //往右移动
    public void RunRight()
    {
        if (currentIndex < 2)
        {
            targetIndex = currentIndex + 1;
            targetPosX = (targetIndex - 1) * 2;
            currentIndex = targetIndex;
        }
    }

    public bool IsGrounded
    {
        get
        {
            return cc.isGrounded;
        }
    }
    //跳
    public void Jump()
    {
        if (cc.isGrounded)
        {
            jumpTimer = 0.537f;
            dropSpeed = 10;
        }
    }
    //持续加速
    void MoveSpeedAdd()
    {
        moveSpeed += speedAddition * Time.deltaTime;
    }

    //减速
    public void LoseSpeed()
    {
        normalSpeed = moveSpeed;
        //速度减半
        moveSpeed = normalSpeed * 0.1f;

    }
    //速度恢复
    void RestoreSpeed()
    {

        if (moveSpeed < normalSpeed)
            moveSpeed = Mathf.MoveTowards(moveSpeed, normalSpeed, 0.2f);

    }
    //打滚
    public void Roll()
    {
        rollTimer = 0.733f;
        cc.center = new Vector3(0, 1f, 0);
        cc.height = 1.5f;
    }
    float rollTimer;
    //站立
    void Stand()
    {
        if (rollTimer < 0)
        {
            cc.center = new Vector3(0, 1.5f, 0);
            cc.height = 3;
        }
        else
        {
            rollTimer -= Time.deltaTime;
        }
    }
    //下坠
    public void Drop()
    {
        jumpTimer = -1;
        dropSpeed = 20;
    }
    #endregion

    #region Unity回调


    void Awake()
    {
        cc = GetComponent<CharacterController>();

        RegisterAttentionEvent();
        startZ = transform.position.z;
    }

    void FixedUpdate()
    {
        if (!isPlaying)
            return;

        //每一帧执行的代码
        Run();
        //向左移动
        if (transform.position.x - targetPosX > 0.01f)
        {
            float x_Distance = transform.position.x;
            x_Distance = Mathf.MoveTowards(x_Distance, targetPosX, (8 / 0.667f) * Time.deltaTime);
            transform.position = new Vector3(x_Distance, transform.position.y, transform.position.z);
        }
        //向右移动
        if (targetPosX - transform.position.x > 0.01f)
        {
            float x_Distance = transform.position.x;
            x_Distance = Mathf.MoveTowards(x_Distance, targetPosX, (8 / 0.667f) * Time.deltaTime);
            transform.position = new Vector3(x_Distance, transform.position.y, transform.position.z);
        }
        //垂直移动
        if (jumpTimer > 0)
        {
            cc.Move(transform.up * 10 * Time.deltaTime);
            jumpTimer -= Time.deltaTime;
        }
        else if (transform.position.y > 0)
        {
            cc.Move(-transform.up * dropSpeed * Time.deltaTime);
            if (transform.position.y < 0)
            {
                transform.position = new Vector3(transform.position.x, 0, transform.position.z);
            }
        }
        MoveSpeedAdd();
        RestoreSpeed();
        Stand();
        float distance = transform.position.z - startZ;
        RuntimeEventArgs data = new RuntimeEventArgs();
        data.effect = RuntimeEvent.Move;
        data.distance = distance;
        MVC.SendEvent(Consts.E_RuntimePassiveEvent, data);
    }
    #endregion

    #region 事件回调
    public override void HandleEvent(string eventName, object data)
    {
        //Debug.Log("HandleEvent:" + eventName);
        switch (eventName)
        {
            case Consts.E_GameStateUpdate:
                GlobalModel args = data as GlobalModel;
                if (args.State == GameState.Play)
                {
                    isPlaying = true;
                }
                else
                {
                    isPlaying = false;
                }
                break;


            default: break;
        }
    }
    #endregion

}
