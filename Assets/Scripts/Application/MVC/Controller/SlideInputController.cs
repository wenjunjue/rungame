﻿
//玩家输入控制器
public class SlideInputController : Controller
{

    public override void Execute(object data)
    {
        //如果不在运行状态，该Controller不工作
        GlobalModel global = GetModel<GlobalModel>();
        if (global.State != GameState.Play)
            return;

        PlayerAnimView anim = GetView<PlayerAnimView>();
        PlayerMoveView move = GetView<PlayerMoveView>();
        SlideInputArgs dir = data as SlideInputArgs;

        switch (dir.direction)
        {
            case "Right":
                if (anim.State != "jump")
                    anim.RunRight();
                move.RunRight();

                break;
            case "Left":
                if (anim.State != "jump")
                    anim.RunLeft();
                move.RunLeft();

                break;
            case "Up":
                anim.Jump();
                move.Jump();
                break;
            case "Down":
                if (anim.State == "jump")
                    move.Drop();
                anim.Roll();
                move.Roll();



                break;
            default: break;

        }

    }
}
