﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInputController : Controller
{
    public override void Execute(object data)
    {
        UIInputArgs args = data as UIInputArgs;
        switch (args.button)
        {
            case "Pause_Continue":
                GetModel<GlobalModel>().State = GameState.Play;
                GetView<PausePanelView>().Hide();
                break;
            case "Pause_Home":
                Root.Instance.LoadScene(1);
                break;
            case "Game_Pause":
                if (GetModel<GlobalModel>().State != GameState.Play)
                    return;
                GetModel<GlobalModel>().State = GameState.Pause;
                GetView<PausePanelView>().Show();
                break;
            
            case "Game_Double":
                GetModel<RuntimeModel>().UseDouble();
                break;
            case "Game_Magnet":
                GetModel<RuntimeModel>().UseMagnet();
                GetView<MagnetView>().UseMagnet();
                break;
            case "Game_Whistle":
                GetModel<RuntimeModel>().UseWhistle();
                break;
            case "Game_Ball":
                GetView<ShootBallView>().Shoot();
                GetModel<RuntimeModel>().UseBall();
                GetView<PlayerAnimView>().PlayShoot();
                break;
            case "Fail_Bribery":
                int cost=GetModel<RuntimeModel>().BriberyPrice;
                GetModel<GlobalModel>().Money -= cost;
                GetModel<GlobalModel>().State = GameState.Play;
                GetView<PlayerAnimView>().PlayRun();
                GetView<FailPanelView>().Hide();
                break;
            case "Fail_End":
                GetView<ScorePanelView>().Show();
                GetView<FailPanelView>().Hide();
                break;
            case "Score_Shop":
                Root.Instance.LoadScene(2);
                break;
            case "Score_Home":
                Root.Instance.LoadScene(1);
                break;
            case "Score_Restart":
                Root.Instance.LoadScene(4);
                break;
            
            case "Shop_Zhuangbei":
                GetModel<GlobalModel>().CharSkinIndex =GetView<ShopPanelView>().CharSkinIndex;
                GetModel<GlobalModel>().BallSkinIndex = GetView<ShopPanelView>().BallSkinIndex;
                Debug.Log("装备完毕");
                break;
            case "Shop_Start":
                Root.Instance.LoadScene(3);
                break;
            case "Shop_Back":
                Root.Instance.LoadScene(1);
                break;
            case "Home_Shop":
                //切换到shop页面
                Root.Instance.LoadScene(2);
                break;
            case "Home_Start":
                //切换到buytool页面
                Root.Instance.LoadScene(3);
                break;
            case "Home_Setting":
                //显示设置面板
                GetView<SettingPanelView>().Show();
                break;
            case "BuyTool_Back":
                Root.Instance.LoadScene(2);
                break;
            case "BuyTool_Start":
                Root.Instance.LoadScene(4);
                break;
            case "BuySuiji":
                Debug.Log("购买随机道具");
                GetModel<RuntimeModel>().GetSuiji();
                break;
            case "BuyWhistle":
                GetModel<RuntimeModel>().GetWhistle();
                break;
            case "BuyMagnet":
                GetModel<RuntimeModel>().GetMagnet();
                break;
            case "BuyDouble":
                GetModel<RuntimeModel>().GetDouble();
                break;
        }
    }

}
public class UIInputArgs
{
    public UIInputArgs(string button)
    {
        this.button = button;
    }
    public string button;
}
