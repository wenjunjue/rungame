﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//退出场景控制器
public class ExitSceneController : Controller {
    public override void Execute(object data)
    {
        ScenesArgs args = data as ScenesArgs;
        if (args.sceneIndex == 1)
        {
            MVC.RemoveView(Consts.V_HomePanelView);
            MVC.RemoveView(Consts.V_SettingPanelView);

        }
        else if (args.sceneIndex == 2) {
            MVC.RemoveView(Consts.V_ShopPanelView);
        }
        else if (args.sceneIndex == 3)
        {

            MVC.RemoveView(Consts.V_BuyToolPanelView);
        }
        else if (args.sceneIndex == 4) {
            
            GetModel<RuntimeModel>().Reset();
            MVC.RemoveView(Consts.V_PlayerMove);
            MVC.RemoveView(Consts.V_PlayerAnim);
            MVC.RemoveView(Consts.V_SlideInput);
            MVC.RemoveView(Consts.V_Magnet);
            MVC.RemoveView(Consts.V_GameTimer);
            MVC.RemoveView(Consts.V_ShootBall);
            MVC.RemoveView(Consts.V_GamePanelView);
            MVC.RemoveView(Consts.V_PausePanelView);
            MVC.RemoveView(Consts.V_FailPanelView);
            MVC.RemoveView(Consts.V_ScorePanelView);
        }

    }

}
