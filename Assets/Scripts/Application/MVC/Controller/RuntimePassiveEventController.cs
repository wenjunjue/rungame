﻿using System.Collections;

using System.Collections.Generic;
using UnityEngine;

public class RuntimePassiveEventController : Controller
{
    public override void Execute(object data)
    {
        //如果不在运行状态，该Controller不工作
        GlobalModel global = GetModel<GlobalModel>();
        if (global.State != GameState.Play)
            return;
        RuntimeEventArgs args = data as RuntimeEventArgs;
        switch (args.effect)
        {
            case RuntimeEvent.LoseSpeed:
                if (GetModel<RuntimeModel>().whistleTimer <= 0)
                {
                    GetView<PlayerMoveView>().LoseSpeed();
                    GetView<PlayerAnimView>().Hit();
                }
                break;
            case RuntimeEvent.Die:
                GetModel<GlobalModel>().State = GameState.Die;
                GetModel<RuntimeModel>().Die();
                GetView<PlayerAnimView>().Die();
                GetView<FailPanelView>().Invoke("Show", 2);
                break;
            case RuntimeEvent.GetMoney:
                GetModel<RuntimeModel>().GetCoin();
                break;
            case RuntimeEvent.GetDouble:
                GetModel<RuntimeModel>().GetDouble();
                break;
            case RuntimeEvent.GetMagnet:
                GetModel<RuntimeModel>().GetMagnet();
                break;
            case RuntimeEvent.GetWhistle:
                GetModel<RuntimeModel>().GetWhistle();
                break;
            case RuntimeEvent.ReadyShoot:
                GetModel<RuntimeModel>().GetBall();
                break;
            case RuntimeEvent.Goal:
                GetModel<RuntimeModel>().GetGoal();
                GetView<ShootBallView>().Init();
                break;
            case RuntimeEvent.GoalKeeper:
                GetView<ShootBallView>().Init();
                break;
            case RuntimeEvent.Move:
                GetModel<RuntimeModel>().GetDistance(args.distance);
                break;
            default:
                break;
        }

    }
}
public enum RuntimeEvent {
    Move,
    GoalKeeper,
    Goal,
    ReadyShoot,
    GetWhistle,
    GetMagnet,
    GetDouble,
    GetMoney,
    Die,
    LoseSpeed
}
public class RuntimeEventArgs {
    public RuntimeEvent effect;
    public float distance;
}