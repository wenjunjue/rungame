﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//启动游戏的经理
public class StartUpController : Controller {
    public override void Execute(object data)
    {


        //注册Model
        MVC.RegisterModel(new GlobalModel());
        MVC.RegisterModel(new RuntimeModel());
        //读取本地存档
        GetModel<GlobalModel>().Read();
        //注册Controller
        MVC.RegisterController(Consts.E_SlideInput, typeof(SlideInputController));
        MVC.RegisterController(Consts.E_EnterScene, typeof(EnterSceneController));
        MVC.RegisterController(Consts.E_ExitScene, typeof(ExitSceneController));
        MVC.RegisterController(Consts.E_RuntimePassiveEvent, typeof(RuntimePassiveEventController));
        MVC.RegisterController(Consts.E_UIInput, typeof(UIInputController));

        //启动游戏
        Root.Instance.LoadScene(1);
        Debug.Log("启动游戏");
    }
}
