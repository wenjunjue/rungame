﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//进入场景控制器
public class EnterSceneController : Controller
{
    public override void Execute(object data)
    {
        ScenesArgs args = data as ScenesArgs;
        if (args.sceneIndex == 1)
        {
            SoundManager.Instance.PlayBG("Bgm_JieMian");
            var canvs = GameObject.Find("Canvas");
            MVC.RegisterView(canvs.transform.Find("HomePanel").GetComponent<HomePanelView>());
            MVC.RegisterView(canvs.transform.Find("SettingPanel").GetComponent<SettingPanelView>());
        }

        else if (args.sceneIndex == 2)
        {
            var canvs = GameObject.Find("Canvas");
            var panel = canvs.transform.Find("ShopPanel");
            MVC.RegisterView(panel.GetComponent<ShopPanelView>());
        }
        else if (args.sceneIndex == 3)
        {
            var canvs = GameObject.Find("Canvas");
            var panel = canvs.transform.Find("BuyToolPanel");
            MVC.RegisterView(panel.GetComponent<BuyToolPanelView>());
        }
        //04-Game
        else if (args.sceneIndex == 4)
        {
            SoundManager.Instance.PlayBG("Bgm_ZhanDou");
            var player = GameObject.FindWithTag(Tags.Player);
            MVC.RegisterView(player.GetComponent<PlayerMoveView>());
            MVC.RegisterView(player.GetComponent<PlayerAnimView>());
            MVC.RegisterView(player.GetComponent<SlideInputView>());
            MVC.RegisterView(player.GetComponent<MagnetView>());
            MVC.RegisterView(player.GetComponent<GameTimerView>());
            var canvs = GameObject.Find("Canvas").transform;
            MVC.RegisterView(canvs.Find("GamePanel").GetComponent<GamePanelView>());
            MVC.RegisterView(canvs.Find("PausePanel").GetComponent<PausePanelView>());
            MVC.RegisterView(canvs.Find("FailPanel").GetComponent<FailPanelView>());
            MVC.RegisterView(canvs.Find("ScorePanel").GetComponent<ScorePanelView>());
            MVC.RegisterView(player.transform.Find("Ball").GetComponent<ShootBallView>());
            GetModel<GlobalModel>().State = GameState.Pause;
            Debug.Log("EnterScene 04-Game");
        }
    }

}
