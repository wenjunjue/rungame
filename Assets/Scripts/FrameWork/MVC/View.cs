﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//销售
public abstract class View : MonoBehaviour {

    //名字
    public abstract string Name { get; }

    //关心事件列表
    [HideInInspector]
    public List<string> AttentionList = new List<string>();

    //注册关心事件，关心事件初始化
    public virtual void RegisterAttentionEvent() { 
    
    }

    //响应事件
    public abstract void HandleEvent(string eventName,object data);

    //发起事件
    public void SendEvent(string eventName, object data = null)
    {
        MVC.SendEvent(eventName, data);
    }

    //获取模型实例
    public T GetModel<T>() where T : Model {
        return MVC.GetModel<T>();
    }
}
