﻿public abstract class Controller
{
    //执行
    public abstract void Execute(object data);


    public T GetModel<T>() where T : Model
    {
        return MVC.GetModel<T>();
    }



    public T GetView<T>() where T : View
    {
        return MVC.GetView<T>();
    }
}

