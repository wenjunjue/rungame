﻿using System;
using System.Collections.Generic;


public static class MVC
{
    //销售人员名单
    public static Dictionary<string, View> viewsDict = new Dictionary<string, View>();//名字，销售人员
    //研发人员名单
    public static Dictionary<string, Model> modelsDict = new Dictionary<string, Model>();//名字，研发人员
    //经理负责事务名单
    public static Dictionary<string, Type> commandMap = new Dictionary<string, Type>();//事务，经理

    //注册View，销售入职
    public static void RegisterView(View view)
    {
        viewsDict[view.Name] = view;
    }
    public static void RemoveView(string name) {
        viewsDict.Remove(name);
    }

    //注册Model，研发入职
    public static void RegisterModel(Model model)
    {
        modelsDict[model.Name] = model;
    }
    //绑定event和Controller，经理开始对某件事负责，接受委任
    public static void RegisterController(string eventName, Type controllerType)
    {
        commandMap[eventName] = controllerType;
    }

    //获取model，抓到一个研发人员
    public static T GetModel<T>() where T : Model
    {
        foreach (var m in modelsDict.Values)
        {
            if (m is T)
            {
                return m as T;
            }
        }
        return null;
    }

    //获取view，抓到一个销售人员
    public static T GetView<T>() where T : View
    {
        foreach (var m in viewsDict.Values)
        {
            if (m is T)
            {
                return m as T;
            }
        }
        return null;
    }


    public static void SendEvent(string eventName, object data = null)
    {
        if (commandMap.ContainsKey(eventName))
        {
            Type t = commandMap[eventName];
            //利用反射生成控制器
            Controller c = Activator.CreateInstance(t) as Controller;
            c.Execute(data);

        }
        foreach (var v in viewsDict.Values)
        {
            if (v!=null&&v.AttentionList.Contains(eventName))
            {
                v.HandleEvent(eventName, data);
            }
        }
    }
}
