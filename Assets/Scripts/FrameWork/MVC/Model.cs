﻿//研发人员
public abstract class Model
{

    //名字
    public abstract string Name { get; }


    //发起事件
    public void SendEvent(string eventName, object data = null)
    {
        MVC.SendEvent(eventName, data);
    }
}
