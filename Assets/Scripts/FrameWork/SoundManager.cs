﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//声音管理
public class SoundManager : MonoSingleton<SoundManager>
{

    AudioSource m_BG;//背景音乐播放器
    AudioSource m_Effect;//特效音乐播放器

    Dictionary<string, AudioClip> m_clips = new Dictionary<string, AudioClip>();//所有音频的缓存
    string ResourcesDir = "Sound";


    protected override void Awake()
    {
        base.Awake();
        //添加背景音乐播放器
        m_BG = gameObject.AddComponent<AudioSource>();
        m_BG.loop = true;//开启循环
        m_BG.playOnAwake = false;//自动播放
        //添加特效音乐播放器
        m_Effect = gameObject.AddComponent<AudioSource>();
    }
    //播放背景音乐
    public void PlayBG(string clipName)
    {
        string oldName;
        if (m_BG.clip == null)
        {
            oldName = "";
        }
        else
        {
            oldName = m_BG.clip.name;
        }
        //可以切换
        if (oldName != clipName)
        {
            AudioClip clip;
            if (!m_clips.ContainsKey(clipName))
            {
                string path = ResourcesDir + "/" + clipName;
                AudioClip c = Resources.Load<AudioClip>(path);
                if (c != null)
                    m_clips.Add(clipName, c);
            }
            clip = m_clips[clipName];
            if (clip != null)
            {
                m_BG.clip = clip;
                m_BG.Play();
            }
        }
    }

    //播放特效音乐
    public void PlayEffect(string clipName)
    {
        AudioClip clip;
        if (!m_clips.ContainsKey(clipName))
        {
            //加载
            string path = ResourcesDir + "/" + clipName;
            AudioClip c = Resources.Load<AudioClip>(path);
            if (c != null)
                m_clips.Add(clipName, c);
            else
                Debug.LogError(path);
        }
        clip = m_clips[clipName];
        if (clip != null)
        {
            m_Effect.PlayOneShot(clip);
        }

    }

    public void SetMusicVolume(float value) {
        m_BG.volume = value;
    }
    public void SetEffectVolume(float value)
    {
        m_Effect.volume = value;
    }
}
