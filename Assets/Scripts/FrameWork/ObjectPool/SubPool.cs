﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubPool
{

    //保存了池子中的所有物体
    List<GameObject> m_objects = new List<GameObject>();

    //池子中物体的预制体
    GameObject m_prefab;

    //池子中物体默认存放的位置
    Transform m_parent;


    //名字
    public string Name
    {
        get
        {
            return m_prefab.name;
        }
    }

    //构造方法
    public SubPool(GameObject prefab, Transform parent)
    {
        m_prefab = prefab;
        m_parent = parent;
    }

    //租用
    public GameObject Spawn()
    {
        GameObject go = null;

        //遍历池子，找到空闲的物体
        for (int i = 0; i < m_objects.Count; i++)
        {
            if (!m_objects[i].activeSelf)
            {
                go = m_objects[i];
                break;
            }
        }
        //如果没有空闲物体，就需要临时生产并对池子扩容
        if (go == null)
        {
            go = GameObject.Instantiate(m_prefab, m_parent);
            m_objects.Add(go);
        }
        //对即将租出去的物体进行初始化
        go.SetActive(true);
        go.SendMessage("OnSpawn", SendMessageOptions.DontRequireReceiver);
        return go;
    }

    //回收单个物体
    public bool UnSpawn(GameObject go)
    {
        if (m_objects.Contains(go))
        {
            go.SendMessage("OnUnSpawn", SendMessageOptions.DontRequireReceiver);
            if (go.transform.parent != m_parent)
                go.transform.SetParent(m_parent);
            go.SetActive(false);
            return true;
        }
        return false;
    }
    //回收所以物体
    public void UnSpawnAll()
    {
        for (int i = 0; i < m_objects.Count; i++)
        {
            if (m_objects[i].activeSelf)
            {
                UnSpawn(m_objects[i]);
            }
        }
    }
}
