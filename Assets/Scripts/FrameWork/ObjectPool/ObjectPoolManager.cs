﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 对象池总管理员
/// </summary>
public class ObjectPoolManager : MonoSingleton<ObjectPoolManager>
{

    //资源目录
    public string ResourceDir = "ObjectPoolPrefab";
    //所有的池子
    Dictionary<string, SubPool> m_pools = new Dictionary<string, SubPool>();


    //租用的逻辑
    public GameObject Spawn(string name)
    {
        GameObject go = null;
        if (!m_pools.ContainsKey(name))
        {
            RegisterNew(name);
        }

        go = m_pools[name].Spawn();

        return go;
    }

    //回收的逻辑
    public bool UnSpawn(GameObject go)
    {
        foreach (var p in m_pools.Values)
        {
            if (p.UnSpawn(go))
            {
                return true;
            }
        }
        return false;
    }

    //回收所有的逻辑
    public void UnSpawnAll()
    {
        foreach (var p in m_pools.Values)
        {
            p.UnSpawnAll();
        }
    }



    //创建新的池子
    SubPool RegisterNew(string name)
    {
        string path = ResourceDir + "/" + name;

        GameObject prefab = Resources.Load<GameObject>(path);
       GameObject parent = new GameObject(name + "Pool");
        parent.transform.SetParent(transform);
        //实例化一个新池子
        SubPool pool = new SubPool(prefab, parent.transform);
        m_pools.Add(name, pool);
        return pool;

    }
}
