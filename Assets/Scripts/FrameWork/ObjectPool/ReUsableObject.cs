﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 所有对象池中的物体都需要挂载该脚本
/// </summary>
public class ReUsableObject : MonoBehaviour, IReUsable
{
    public virtual void OnSpawn() {

    }
    //当一个方法需要不同实现方式 
    public virtual void OnUnSpawn() {

    }

}

/// <summary>
/// 实现物体重复使用，接口
/// </summary>
public interface IReUsable
{
    //当租的时候
    void OnSpawn();
    //当还的时候
    void OnUnSpawn();
}
